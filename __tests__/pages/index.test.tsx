import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import React from 'react'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ContextProvider } from '../../context'
import Index from '../../pages/index'

const queryClient = new QueryClient()

describe('Index page', () => {
  it('should render', () => {
    render(
      <ContextProvider>
        <QueryClientProvider client={queryClient}>
          <Index />
        </QueryClientProvider>
      </ContextProvider>
    )
    const main = screen.getByRole('main')
    expect(main).toBeInTheDocument()
  })
})
