import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import React from 'react'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ContextProvider } from '../../context'
import Help from '../../pages/help'

const queryClient = new QueryClient()

describe('Help page', () => {
  it('should render', () => {
    render(
      <ContextProvider>
        <QueryClientProvider client={queryClient}>
          <Help />
        </QueryClientProvider>
      </ContextProvider>
    )
    const main = screen.getByRole('main')
    expect(main).toBeInTheDocument()
  })
})
