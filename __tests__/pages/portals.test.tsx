import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import React from 'react'
import { mockAllIsIntersecting } from 'react-intersection-observer/test-utils'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ContextProvider } from '../../context'
import Portals from '../../pages/portals'

const queryClient = new QueryClient()

describe('Portals page', () => {
  it('should render', () => {
    render(
      <ContextProvider>
        <QueryClientProvider client={queryClient}>
          <Portals />
        </QueryClientProvider>
      </ContextProvider>
    )

    mockAllIsIntersecting(true)

    const main = screen.getByRole('main')
    expect(main).toBeInTheDocument()
  })
})
