import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import React from 'react'
import { ContextProvider } from '../../context'
import Terms from '../../pages/terms'

describe('Terms page', () => {
  it('should render', () => {
    render(
      <ContextProvider>
        <Terms />
      </ContextProvider>
    )
    const main = screen.getByRole('main')
    expect(main).toBeInTheDocument()
  })
})
