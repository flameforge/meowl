import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import React from 'react'
import { mockAllIsIntersecting } from 'react-intersection-observer/test-utils'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ContextProvider } from '../../context'
import Gold from '../../pages/gold'

const queryClient = new QueryClient()

describe('Gold page', () => {
  it('should render', () => {
    render(
      <ContextProvider>
        <QueryClientProvider client={queryClient}>
          <Gold />
        </QueryClientProvider>
      </ContextProvider>
    )

    mockAllIsIntersecting(true)

    const main = screen.getByRole('main')
    expect(main).toBeInTheDocument()
  })
})
