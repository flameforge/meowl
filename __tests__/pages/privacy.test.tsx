import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import React from 'react'
import { ContextProvider } from '../../context'
import Privacy from '../../pages/privacy'

describe('Privacy page', () => {
  it('should render', () => {
    render(
      <ContextProvider>
        <Privacy />
      </ContextProvider>
    )
    const main = screen.getByRole('main')
    expect(main).toBeInTheDocument()
  })
})
