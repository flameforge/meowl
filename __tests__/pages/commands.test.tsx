import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import React from 'react'
import { ContextProvider } from '../../context'
import { meows } from '../../data/meows'
import { portals } from '../../data/portals'
import Commands from '../../pages/commands'

describe('Commands page', () => {
  it('should render', () => {
    const uniqueList = new Set()

    meows.forEach((meow) => meow.phrases.forEach((phrase) => uniqueList.add(phrase)))
    portals.forEach((portal) => uniqueList.add(portal.name))

    const toSort = Array.from(uniqueList)
    const sorted = toSort.sort()

    render(
      <ContextProvider>
        <Commands sorted={sorted} portals={portals} />
      </ContextProvider>
    )
    const main = screen.getByRole('main')
    expect(main).toBeInTheDocument()
  })
})
