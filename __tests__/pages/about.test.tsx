import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import React from 'react'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ContextProvider } from '../../context'
import About from '../../pages/about'

const queryClient = new QueryClient()

describe('About page', () => {
  it('should render', () => {
    render(
      <ContextProvider>
        <QueryClientProvider client={queryClient}>
          <About />
        </QueryClientProvider>
      </ContextProvider>
    )
    const main = screen.getByRole('main')
    expect(main).toBeInTheDocument()
  })
})
