import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import React from 'react'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ContextProvider } from '../../context'
import Directory from '../../pages/directory'

const queryClient = new QueryClient()

describe('Directory page', () => {
  it('should render', () => {
    render(
      <ContextProvider>
        <QueryClientProvider client={queryClient}>
          <Directory />
        </QueryClientProvider>
      </ContextProvider>
    )
    const main = screen.getByRole('main')
    expect(main).toBeInTheDocument()
  })
})
