import { Container } from '@mui/material'
import '@testing-library/jest-dom'
import { render } from '@testing-library/react'
import React from 'react'
import BackTop, { scrollToTop } from '../../assets/app/backtop'
import { ContextProvider } from '../../context'

describe('App / Backtop component', () => {
  it('should render', () => {
    const scrollToSpy = jest.fn()
    global.scrollTo = scrollToSpy

    render(
      <ContextProvider>
        <Container sx={{ height: '2400px', width: '70px' }}>
          <BackTop />
        </Container>
      </ContextProvider>
    )

    scrollToTop()

    expect(scrollToSpy).toHaveBeenCalled()
  })
})
