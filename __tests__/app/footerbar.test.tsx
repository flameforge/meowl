import '@testing-library/jest-dom'
import { render } from '@testing-library/react'
import React from 'react'
import { QueryClient, QueryClientProvider } from 'react-query'
import Footerbar from '../../assets/app/footerbar'
import { ContextProvider } from '../../context'

const queryClient = new QueryClient()

describe('App / Footerbar component', () => {
  it('should render', () => {
    render(
      <ContextProvider>
        <QueryClientProvider client={queryClient}>
          <Footerbar />
        </QueryClientProvider>
      </ContextProvider>
    )
  })
})
