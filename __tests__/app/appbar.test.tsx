import '@testing-library/jest-dom'
import { render } from '@testing-library/react'
import React from 'react'
import MenuAppBar from '../../assets/app/appbar'
import { ContextProvider } from '../../context'

describe('App / Appbar component', () => {
  it('should render', () => {
    render(
      <ContextProvider>
        <MenuAppBar />
      </ContextProvider>
    )
  })
})
