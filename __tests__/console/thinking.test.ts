import { act } from '../../assets/console/thinking'
import { decode } from './../../assets/console/thinking'
import { fakeMeow, fakePortal } from './mocks'

describe('Thinking / act', () => {
  it('should execute the function command of the object passed in', () => {
    const spy = jest.fn()

    const mock = { ...fakeMeow, command: spy }
    act(mock)
    expect(spy).toHaveBeenCalled()
  })
})

describe('Thinking / decode', () => {
  it('should decode a URI in case it is an URI', () => {
    const encodedURI = 'https:www.meow.test/q=?%20'
    const decodedURI = 'https:www.meow.test/q=? '

    expect(decode(encodedURI)).toEqual(decodedURI)
  })
})

describe('Thinking / convertPortalToMeow', () => {
  it('should convert a Portal object into a Meow object', () => {
    const mock = { ...fakePortal }
    const expectedObject = {
      description: 'Google Search (known simply as Google), is a search engine provided by Google. Where everything used to start in the past.',
      icon: 'https://icons.duckduckgo.com/ip3/www.google.com.ico',
      name: 'Google',
      prefix: 'search?q=',
      searcheable: true,
      tags: ['everything', 'images', 'videos'],
      url: 'https://www.google.com/',
    }

    expect(mock).toEqual(expectedObject)
  })
})
