import mockRouter from 'next-router-mock'
import { story } from '../../assets/app/story'
import * as Human from '../../assets/console/human'

jest.mock('next/dist/client/router', () => require('next-router-mock'))

describe('Console / Human', () => {
  beforeEach(() => {
    mockRouter.setCurrentUrl('/')
  })

  describe('fallback action', () => {
    it('should fallback into the default action', () => {
      const spyFallBack = jest.spyOn(Human, 'fallBack')

      const _DontUnderstand = (story.human.saysDontUnderstand = jest.fn())
      const _DefaultAction = (story.human.saysDefaultAction = jest.fn())
      const _OpensDoorToOutside = (story.human.opensDoorToOutside = jest.fn())
      const _CatJumps = (story.cat.jumps = jest.fn())

      const phrase = 'abc'
      Human.fallBack(phrase)
      expect(spyFallBack).toHaveBeenCalled()

      expect(_DontUnderstand).toHaveBeenCalled()
      expect(_DefaultAction).toHaveBeenCalled()
      expect(_OpensDoorToOutside).toHaveBeenCalled()
      expect(_CatJumps).toHaveBeenCalled()
    })
  })

  describe('default action', () => {
    it('should run the default action', () => {
      const spyDefaultAction = jest.spyOn(Human, 'defaultAction')

      const _DefaultAction = (story.human.saysDefaultAction = jest.fn())
      const _OpensDoorToOutside = (story.human.opensDoorToOutside = jest.fn())
      const _CatJumps = (story.cat.jumps = jest.fn())

      const phrase = 'abc'
      Human.defaultAction(phrase)
      expect(spyDefaultAction).toHaveBeenCalled()

      expect(_DefaultAction).toHaveBeenCalled()
      expect(_OpensDoorToOutside).toHaveBeenCalled()
      expect(_CatJumps).toHaveBeenCalled()
    })
  })
})
