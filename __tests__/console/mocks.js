export const fakeMeow = {
  phrases: ['dir', 'Directory', 'pages'],
  name: 'Directory page',
  description: 'This command opens the Directory page.',
  command: undefined,
}

export const fakePortal = {
  searcheable: true,
  name: 'Google',
  description: 'Google Search (known simply as Google), is a search engine provided by Google. Where everything used to start in the past.',
  url: 'https://www.google.com/',
  prefix: 'search?q=',
  icon: 'https://icons.duckduckgo.com/ip3/www.google.com.ico',
  tags: ['everything', 'images', 'videos'],
}
