import '@testing-library/jest-dom'
import { googleSearchSuggestions, youtubeImgApi } from './../assets/app/external'

describe('External links', () => {
  it('should have a fixed Youtube Images API url', () => {
    expect(youtubeImgApi).toEqual('https://img.youtube.com/vi/')
  })
  it('should have a fixed Google Search Suggestions API url', () => {
    expect(googleSearchSuggestions).toEqual('http://suggestqueries.google.com/complete/search?q=')
  })
})
