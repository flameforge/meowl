import fonts from '../../assets/visuals/fonts.js'

describe('Visuals / Global fonts', () => {
  it('should not have an empty array of font-faces', () => {
    const sample = fonts
    const empty = sample.length <= 0
    expect(empty).toBe(false)
  })
})
