import nyan from '../../assets/visuals/nyan'

describe('Visuals / Nyan Cat Favicon', () => {
  it('should not be an empty array of svgs', () => {
    const sample = nyan
    const empty = sample.length <= 0
    expect(empty).toBe(false)
  })
})
