import isValidHttpUrl from '../../assets/utils/isValidHttpUrl'

describe('Util / isValidHttpUrl', () => {
  it('should tell if it is a valid URL', () => {
    const sample = 'https://meowl.space/'
    const urlTested = isValidHttpUrl(sample)
    expect(urlTested).toBe(true)
  })
  it('should tell if it is an invalid URL', () => {
    const sample = 'meow/'
    const urlTested = isValidHttpUrl(sample)
    expect(urlTested).toBe(false)
  })
})
