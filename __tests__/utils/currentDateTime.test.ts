import currentDateTime from '../../assets/utils/currentDateTime'

describe('Util / currentDateTime', () => {
  it('should return the current time and date', () => {
    const now = new Date()
    const generatedDate = currentDateTime(now)
    const standard =
      now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds() + ' @ ' + now.getDate() + '/' + (now.getMonth() + 1) + '/' + now.getFullYear()
    expect(generatedDate).toEqual(standard)
  })
  it('should return the current time and date with default value', () => {
    const now = new Date()
    const generatedDate = currentDateTime()
    const standard =
      now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds() + ' @ ' + now.getDate() + '/' + (now.getMonth() + 1) + '/' + now.getFullYear()
    expect(generatedDate).toEqual(standard)
  })
})
