import miavatars from '../../assets/utils/miavatars'

describe('Util / miavatars', () => {
  it('should load first miavatar image', () => {
    const sample = miavatars(1)
    expect(sample).toEqual('art/miavatars/meow1.png')
  })

  it('should load first miavatar image on the 11th item', () => {
    const sample = miavatars(11)
    expect(sample).toEqual('art/miavatars/meow1.png')
  })
})
