import alreadyThere from '../../assets/utils/alreadyHere'

describe('Util / alreadyHere', () => {
  beforeEach(() => {
    global.window = Object.create(window)
    const url = 'http://dummy.com'
    Object.defineProperty(window, 'location', {
      value: {
        href: url,
      },
    })
  })

  it('should tell you that you are already in the target URL', () => {
    const targetURL = 'http://dummy.com'
    const here = alreadyThere(targetURL)
    expect(here).toBe(true)
  })

  it('should tell you that you are not already in the target URL', () => {
    const targetURL = 'http://random'
    const here = alreadyThere(targetURL)
    expect(here).toBe(false)
  })
})
