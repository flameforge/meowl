import denoise from '../../assets/utils/denoise'

describe('Util / denoise', () => {
  it('should remove leading and trailing spaces and lowercase everything', () => {
    const sample = ' aA '
    const denoised = denoise(sample)
    expect(denoised).toBe('aa')
  })
})
