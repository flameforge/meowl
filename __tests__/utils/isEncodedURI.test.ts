import isEncoded from '../../assets/utils/isEncodedURI'

describe('Util / isEncodedURI', () => {
  it('should return false as default', () => {
    const encoded = isEncoded()
    expect(encoded).toBe(false)
  })
  it('should tell that it is an encoded URI', () => {
    const sample = 'https://meowl.space/q=?Hello%20G%C3%BCnter'
    const encoded = isEncoded(sample)
    expect(encoded).toBe(true)
  })
  it('should tell that it is not an encoded URI', () => {
    const sample = 'https://meowl.space/q='
    const encoded = isEncoded(sample)
    expect(encoded).toBe(false)
  })
})
