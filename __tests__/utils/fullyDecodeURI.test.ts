import fullyDecodeURI from '../../assets/utils/fullyDecodeURI'

describe('Util / fullyDecodeURI', () => {
  it('should not decode if it is not an URI', () => {
    const sample = 'blah blah'
    const decoded = fullyDecodeURI(sample)
    expect(decoded).toBe(sample)
  })
  it('should decode an encoded URI', () => {
    const sample = 'https%3A%2F%2Fwww.twitter.com'
    const decoded = fullyDecodeURI(sample)
    expect(decoded).toBe('https://www.twitter.com')
  })
  it('should decode a double encoded URI', () => {
    const sample = 'https%253A%252F%252Fwww.twitter.com'
    const decoded = fullyDecodeURI(sample)
    expect(decoded).toBe('https://www.twitter.com')
  })
})
