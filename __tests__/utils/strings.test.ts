import { lowerCaseEverything, removeSpacesAtStartAndEnd } from '../../assets/utils/strings'

describe('Util / strings', () => {
  describe('removeSpacesAtStartAndEnd', () => {
    it('should trim the spaces at the beginning of a string', () => {
      const sample = ' abc'
      const trimmedAtStart = removeSpacesAtStartAndEnd(sample)
      expect(trimmedAtStart).toEqual('abc')
    })

    it('should trim the spaces at the end of a string', () => {
      const sample = 'abc '
      const trimmedAtEnd = removeSpacesAtStartAndEnd(sample)
      expect(trimmedAtEnd).toEqual('abc')
    })
  })

  describe('lowerCaseEverything', () => {
    it('should lowercase the whole string', () => {
      const sample = 'AbC'
      const lowerCased = lowerCaseEverything(sample)
      expect(lowerCased).toEqual('abc')
    })
  })
})
