import { clearConsole } from '../../assets/console/console'
import { goToPage, openDoorToPreviousRoom, turnTheLightsOffAndOn } from '../../assets/console/human'
import { Meow } from '../meows'

export const publicMeows: Meow[] = [
  {
    phrases: ['dir', 'Directory', 'pages'],
    name: 'Directory page',
    description: 'This command opens the Directory page.',
    command: () => goToPage('Directory'),
  },
  {
    phrases: ['ls', 'commands', 'meows'],
    name: 'Commands page',
    description: 'This command opens the Commands page.',
    command: () => goToPage('Commands'),
  },
  {
    phrases: ['gold'],
    name: 'Gold page',
    description: 'This command opens the Gold page.',
    command: () => goToPage('Gold'),
  },
  {
    phrases: ['h', 'home'],
    name: 'Home page',
    description: 'This command opens the Home page.',
    command: () => goToPage('Home'),
  },
  {
    phrases: ['help', 'meow', '?'],
    name: 'Help page',
    description: 'This command opens the Help page.',
    command: () => goToPage('Help'),
  },
  {
    phrases: ['about', 'what is this?'],
    name: 'About page',
    description: 'This command opens the About page.',
    command: () => goToPage('About'),
  },
  {
    phrases: ['l', 'list', 'album', 'portals'],
    name: 'Portals page',
    description: 'This command opens the Portals page.',
    command: () => goToPage('Portals'),
  },
  {
    phrases: ['terms', 'Terms of use'],
    name: 'Terms of use page',
    description: 'This command opens the Terms of use page.',
    command: () => goToPage('Terms'),
  },
  {
    phrases: ['privacy', 'Privacy Policy'],
    name: 'Privacy policy page',
    description: 'This command opens the Privacy policy page.',
    command: () => goToPage('Privacy'),
  },
  {
    phrases: ['reload', 'rl', 'f5'],
    name: 'Reload the page',
    description: 'This command reloads the page, emulates an f5.',
    command: () => turnTheLightsOffAndOn(),
  },
  {
    phrases: ['b', 'back'],
    name: 'Go back',
    description: 'This command goes back to the previous page, emulates a browser-back.',
    command: () => openDoorToPreviousRoom(),
  },
  {
    phrases: ['f', 'forward'],
    name: 'Go forward',
    description: 'This command goes back to the previous page, emulates a browser-forward.',
    command: () => openDoorToPreviousRoom(),
  },
  {
    phrases: ['clear', 'cls'],
    name: 'Clear the console',
    description: 'This command clears the Console in developer tools.',
    command: () => clearConsole(),
  },
]
