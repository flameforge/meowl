import { meowSuggestions } from './meows'

export const getOptionsAsync = (query: string) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      const suggestions = meowSuggestions(query)
      resolve(suggestions.filter((o) => o.name.toLowerCase().indexOf(query.toLowerCase()) > -1))
    }, 0)
  })
}
