import { convertPortalToMeow } from '../assets/console/thinking'
import denoise from '../assets/utils/denoise'
import { portalsContainingName, portalsWithName } from './portals'
import { privateMeows } from './private/meows'
import { publicMeows } from './public/meows'

export interface Meow {
  phrases: string[]
  name?: string
  description?: string
  command?: Function
  avatar?: string
  alias?: Meow
}

export const meows = [...publicMeows, ...privateMeows]

export const meowsWithPhrase = (phrase: string): Meow[] => meows.filter(({ phrases: x }) => x.includes(phrase))

export const meowsStartingWithPhrase = (phrase: string): Meow[] =>
  meows.filter((meow) => meow.phrases.filter((x) => x.startsWith(denoise(phrase))).length > 0)

export const meowsWithName = (name: string): Meow[] => meows.filter(({ name: x }) => denoise(x).includes(name))

export const meowsFromPortalName = (message: string): Meow[] => portalsWithName(message).map((portal) => convertPortalToMeow(portal))

export const meowsFromPortalNameContainingPhrase = (name: string): Meow[] => portalsContainingName(name).map((portal) => convertPortalToMeow(portal))

export const meowSuggestions = (phrase: string) => {
  if (phrase.length < 1) return []

  const a = meowsStartingWithPhrase(phrase)
  const b = meowsWithName(phrase)

  // const c = [];
  // const c = meowsFromPortalNameContainingPhrase(phrase);

  const allMeows = [...a, ...b]
  const allMeowsDeduped = allMeows.filter((v, i, a) => a.findIndex((t) => t.name === v.name) === i)

  return allMeowsDeduped
}
