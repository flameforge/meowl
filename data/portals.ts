import denoise from '../assets/utils/denoise'
import { privatePortals } from './private/portals'
import { publicPortals } from './public/portals'

export interface Portal {
  name: string
  url: string
  icon: string
  description: string
  searcheable: boolean
  prefix?: string
  suffix?: string
  tags?: string[]
}

export const portals = [...publicPortals, ...privatePortals]

export const portalWithName = (name: string): Portal => portals.find(({ name: x }) => denoise(x) === denoise(name))

export const portalsWithName = (name: string): Portal[] => portals.filter(({ name: x }) => denoise(x) === denoise(name))

export const portalsStartingWithName = (name: string): Portal[] => portals.filter(({ name: x }) => denoise(x).startsWith(denoise(name)))

export const portalsContainingName = (name: string): Portal[] => portals.filter(({ name: x }) => denoise(x).indexOf(denoise(name)) > 1)

export const portalsWithNames = (names: string[]): Portal[] => portals.filter(({ name }) => names.map((x) => denoise(x)).includes(denoise(name)))
