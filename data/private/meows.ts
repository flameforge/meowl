import { goToPortal } from '../../assets/console/human'
import { Meow } from '../meows'
import { portalWithName } from '../portals'

export const privateMeows: Meow[] = [
  {
    phrases: ['kitten', 'placeholder'],
    name: 'Placekitten',
    description: 'This command opens the Placekitten portal',
    command: () => goToPortal(portalWithName('Placekitten')),
  },
  {
    phrases: ['wiki'],
    name: 'Wikipedia',
    description: 'This command opens the Wikipedia portal',
    command: () => goToPortal(portalWithName('Wikipedia')),
  },
  {
    phrases: ['gm'],
    name: 'Google Email',
    description: 'This command opens the Gmail portal',
    command: () => goToPortal(portalWithName('Gmail')),
  },
  {
    phrases: ['gm'],
    name: 'Google Analytics',
    description: 'This command opens Google Analytics',
    command: () => goToPortal(portalWithName('Google Analytics')),
  },
  {
    phrases: ['yt'],
    name: 'Youtube',
    description: 'This command opens Youtube',
    command: () => goToPortal(portalWithName('Youtube')),
  },
  {
    phrases: ['tr'],
    name: 'Google Translator',
    description: 'This command opens the Google Translate portal',
    command: () => goToPortal(portalWithName('Google Translator')),
  },
  {
    phrases: ['cal'],
    name: 'Google Calendar',
    description: 'This command opens Google Calendar',
    command: () => goToPortal(portalWithName('Google Calendar')),
  },
  {
    phrases: ['calc'],
    name: 'Desmos Calculator',
    description: 'This command opens the Desmos normal calculator',
    command: () => goToPortal(portalWithName('Desmos Calculator')),
  },
  {
    phrases: ['fb'],
    name: 'Facebook',
    description: 'This command opens Facebook',
    command: () => goToPortal(portalWithName('Facebook')),
  },
  {
    phrases: ['lab'],
    name: 'Gitlab',
    description: 'This command opens Gitlab',
    command: () => goToPortal(portalWithName('Gitlab')),
  },
  {
    phrases: ['git'],
    name: 'Github',
    description: 'This command opens Github',
    command: () => goToPortal(portalWithName('Github')),
  },
]
