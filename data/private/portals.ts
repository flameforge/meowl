import { Portal } from '../portals'

export const privatePortals: Portal[] = [
  {
    name: 'Placekitten',
    url: 'https://placekitten.com/',
    icon: 'https://placekitten.com/g/40/40',
    description: 'A quick and simple service for getting pictures of kittens for use as placeholders in your designs or code.',
    searcheable: false,
    tags: ['developers', 'images', 'cats'],
  },
]
