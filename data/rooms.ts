import denoise from '../assets/utils/denoise'

export interface Room {
  name: string
  path: string
}

export const room = (name: string) =>
  rooms.find((x) => {
    return denoise(x.name) === denoise(name)
  })

export const rooms: Room[] = [
  {
    name: 'Directory page',
    path: '/directory',
  },
  {
    name: 'Commands',
    path: '/commands',
  },
  {
    name: 'Home',
    path: '/',
  },
  {
    name: 'Help',
    path: '/help',
  },
  {
    name: 'About',
    path: '/about',
  },
  {
    name: 'Portals',
    path: '/portals',
  },
  {
    name: 'Gold',
    path: '/gold',
  },
  {
    name: 'Terms',
    path: '/terms',
  },
  {
    name: 'Privacy',
    path: '/privacy',
  },
]
