/* eslint-disable no-irregular-whitespace */
import catjump from '../public/art/ascii/catjump.txt'
import catleaves from '../public/art/ascii/catleaves.txt'
import meowjumps from '../public/art/ascii/meowjumps.txt'
import meowr from '../public/art/ascii/meowr.txt'
import turnsback from '../public/art/ascii/turnsback.txt'

export interface Ascii {
  id: string
  name: string
  description: string
  content: string
  labels?: string[]
}

export const ascii = (id: string) => asciis.find((art) => art.id === id).content

export const asciis: Ascii[] = [
  {
    id: 'meowjumps',
    name: 'Meowl jumps',
    description: 'Meowers intro art for the developers console in browsers',
    content: meowjumps,
  },
  {
    id: 'catjump',
    name: 'Cat jump',
    description: 'A very small depiction of a cat jumping',
    content: catjump,
  },
  {
    id: 'meowr',
    name: 'mmeeoowwrr!',
    description: 'A very small cat saying meowr',
    content: meowr,
  },
  {
    id: 'turnsback',
    name: 'back',
    description: 'A very small cat turn his back on you, as you deserve.',
    content: turnsback,
  },
  {
    id: 'catleaves',
    name: 'Cat Leaves',
    description: 'A small cat running away from you',
    content: catleaves,
  },
]
