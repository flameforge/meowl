# Privacy Notice

Last updated November 19, 2021

Thank you for choosing to be part of our community at Marlucio's meow jumps! We are committed to protecting your personal information and your right to privacy. If you have any questions or concerns about this privacy notice or our practices concerning your personal information, please get in touch with us at brunopalma@gmail.com.

This privacy notice describes how we might use your information if you:

- Visit our website at <http://www.meowl.com>
- Engage with us in other related ways ― including any sales, marketing, or events

In this privacy notice, if we refer to:

- "**Website**," we are referring to any website of ours that references or links to this policy
- "**Services**," we are referring to our website, and other related services, including any sales, marketing, or events

The purpose of this privacy notice is to explain to you in the most straightforward way possible what information we collect, how we use it, and what rights you have to it. If there are any terms in this privacy notice that you disagree with, please discontinue use of our Services immediately.

**Please read this privacy notice carefully, as it will help you understand what we do with the information that we collect.**

## Table of contents

- [Privacy Notice](#privacy-notice)
  - [Table of contents](#table-of-contents)
  - [1. What information do we collect?](#1-what-information-do-we-collect)
    - [The personal information you disclose to us](#the-personal-information-you-disclose-to-us)
    - [Information automatically collected](#information-automatically-collected)
  - [2. How do we use your information?](#2-how-do-we-use-your-information)
  - [3. Will your information be shared with anyone?](#3-will-your-information-be-shared-with-anyone)
  - [4. Do we use cookies and other tracking technologies?](#4-do-we-use-cookies-and-other-tracking-technologies)
  - [5. How long do we keep your information?](#5-how-long-do-we-keep-your-information)
  - [6. What are your privacy rights?](#6-what-are-your-privacy-rights)
  - [7. Controls for do-not-track features](#7-controls-for-do-not-track-features)
  - [8. Do California residents have specific privacy rights?](#8-do-california-residents-have-specific-privacy-rights)
  - [CCPA Privacy Notice](#ccpa-privacy-notice)
    - [What categories of personal information do we collect?](#what-categories-of-personal-information-do-we-collect)
    - [How do we use and share your personal information?](#how-do-we-use-and-share-your-personal-information)
    - [Will your information be shared with anyone else](#will-your-information-be-shared-with-anyone-else)
    - [Your rights concerning your data](#your-rights-concerning-your-data)
      - [Right to request deletion of the data - Request to delete](#right-to-request-deletion-of-the-data---request-to-delete)
    - [Right to be informed - Request to know](#right-to-be-informed---request-to-know)
    - [Verification process](#verification-process)
    - [Other privacy rights](#other-privacy-rights)
  - [9. Do we make updates to this notice](#9-do-we-make-updates-to-this-notice)
  - [10. How can you contact us about this notice?](#10-how-can-you-contact-us-about-this-notice)
  - [11. How can you review, update, or delete the data we collect from you?](#11-how-can-you-review-update-or-delete-the-data-we-collect-from-you)

## 1. What information do we collect?

### The personal information you disclose to us

**In Short:** _We collect personal information that you provide to us._

We collect personal information that you voluntarily provide to us when you express an interest in obtaining information about us or our products and Services, when you participate in activities on the website, or otherwise when you contact us.

The personal information that we collect depends on the context of your interactions with us and the website, the choices you make, and the products and features you use. The personal information we collect may include the following:

All personal information you provide to us must be true, complete, and accurate, and you must notify us of any changes to such personal information.

### Information automatically collected

**In Short:** _Some information — such as your Internet Protocol (IP) address and/or browser and device characteristics — is collected automatically when you visit our website._

We automatically collect certain information when you visit, use or navigate the website. This information does not reveal your specific identity (like your name or contact information). Still, it may include device and usage information, such as your IP address, browser, device characteristics, operating system, language preferences, referring URLs, device name, country, location, information about how and when you use our website, and others technical information. This information is primarily needed to maintain the security and operation of our website and for our internal analytics and reporting purposes.

Like many businesses, we also collect information through cookies and similar technologies.

The information we collect includes:

- Log and Usage Data. Log and usage data are service-related, diagnostic, usage, and performance information our servers automatically collect when you access or use our website and which we record in log files. Depending on how you interact with us, this log data may include your IP address, device information, browser type, and settings, and information about your activity on the website (such as the date/time stamps associated with your usage, pages, and files viewed, searches and other actions you take such as which features you use), device event information (such as system activity, error reports (sometimes called 'crash dumps') and hardware settings).

- Device Data. We collect device data such as information about your computer, phone, tablet, or other devices you use to access the website. Depending on the device used, this data may include your IP address (or proxy server), device and application identification numbers, location, browser type, hardware model, Internet service provider and/or mobile carrier, operating system, and system configuration information.
- Location Data. We collect location data, such as information about your device's location, which can be precise or imprecise. How much information we collect depends on the type and settings of the device you use to access the website. For example, we may use GPS and other technologies to collect geolocation data that tells us your current location (based on your IP address). You can opt-out of allowing us to collect this information either by refusing access to the information or by disabling your Location setting on your device. Note, however, if you choose to opt-out, you may not use certain aspects of the Services.

## 2. How do we use your information?

**In Short:** _We process your information for purposes based on legitimate business interests, the fulfillment of our contract with you, compliance with our legal obligations, and/or your consent._

We use personal information collected via our website for a variety of business purposes described below. We process your data for these purposes in reliance on our legitimate business interests to enter into or perform a contract with you, with your consent, and/or for compliance with our legal obligations. We indicate the specific processing grounds we rely on next to each purpose listed below.

We use the information we collect or receive:

- **To facilitate account creation and logon process.** For example, suppose you choose to link your account with us to a third-party account (such as your Google or Facebook account). In that case, we use the information you allowed us to collect from those third parties to facilitate account creation and logon process for the performance of the contract.

- **To post testimonials.** We post testimonials on our website that may contain personal information. Before posting a testimonial, we will obtain your consent to use your name and the content of the testimonial. If you wish to update or delete your testimonial, please get in touch with us at brunopalma@gmail.com and be sure to include your name, testimonial location, and contact information.

- **Request feedback.** We may use your information to request feedback and to contact you about your use of our website.

- **To enable user-to-user communications.** We may use your information to help user-to-user communications with each user's consent.

- **To manage user accounts.** We may use your information to operate our performance and keep it in working order.

- **To send administrative information to you.** For example, we may use your personal information to send you product, service, and new feature information and/or information about changes to our terms, conditions, and policies.

- **To protect our Services.** We may use your information as part of our efforts to keep our website safe and secure (for example, for fraud monitoring and prevention).

- **To enforce our terms, conditions, and policies for business purposes, comply with legal and regulatory requirements, or connect with our contract.**

- **To respond to legal requests and prevent harm.** For example, if we receive a subpoena or other legal request, we may need to inspect the data we hold to determine how to respond.

- **Fulfill and manage your orders.** We may use your information to fulfill and manage your orders, payments, returns, and exchanges made through the website.

- **Administer prize draws and competitions.** We may use your information to administer prize draws and competitions when you elect to participate in our contests.

- **To deliver and facilitate the delivery of services to the user.** We may use your information to provide you with the requested service.

- **To respond to user inquiries/offer support to users.** We may use your information to respond to your questions and solve any potential issues you might have with the use of our Services.

- **To send you marketing and promotional communications.** We and/or our third-party marketing partners may use the personal information you send to us for our marketing purposes per your marketing preferences. For example, we will collect personal information from you when expressing an interest in obtaining information about us or our website, subscribing to marketing, or otherwise contacting us. You can opt out of our marketing emails at any time (see the "WHAT ARE YOUR PRIVACY RIGHTS?" below).

- **Deliver targeted advertising to you.** For example, we may use your information to develop and display personalized content and advertising (and work with third parties who do so) tailored to your interests and/or location and to measure its effectiveness.

## 3. Will your information be shared with anyone?

**In Short:** _We only share information with your consent, to comply with laws, to provide you with services, to protect your rights, or to fulfill business obligations._

We may process or share the data that we hold based on the following legal basis:

- **Consent:** We may process your data if you have given us specific permission to use your personal information for a particular purpose.

- **Legitimate Interests:** We may process your data reasonably necessary to achieve our legitimate business interests.

- **Performance of a Contract:** Where we have entered into a contract with you, we may process your personal information to fulfill the terms of our agreement.

- **Legal Obligations:** We may disclose your information where we are legally required to do so to comply with applicable law, governmental requests, a judicial proceeding, court order, or legal processes, such as in response to a court order or a subpoena (including in response to public authorities to meet national security or law enforcement requirements).

- **Vital Interests:** We may disclose your information where we believe it is necessary to investigate, prevent, or take action regarding potential violations of our policies, suspected fraud, situations involving potential threats to the safety of any person and illegal activities, or as evidence in litigation in which we are involved.

More specifically, we may need to process your data or share your personal information in the following situations:

- **Business Transfers.** We may share or transfer your information in connection with, or during negotiations of, any merger, sale of company assets, financing, or acquisition of all or a portion of our business to another company.

## 4. Do we use cookies and other tracking technologies?

**In Short:** _We may use cookies and other tracking technologies to collect and store your information._

We may use cookies and similar tracking technologies (like web beacons and pixels) to access or store information. Our Cookie Notice sets out specific information about how we use such technologies and how you can refuse certain cookies.

## 5. How long do we keep your information?

**In Short:** _We keep your information for as long as necessary to fulfill the purposes outlined in this privacy notice unless otherwise required by law._

We will only keep your personal information for as long as it is necessary for the purposes set out in this privacy notice unless a more extended retention period is required or permitted by law (such as tax, accounting, or other legal requirements). No purpose in this notice will need us to keep your personal information for longer than two years.

When we have no ongoing legitimate business need to process your personal information, we will either delete or anonymize such data or if this is not possible (for example, because your data has been stored in backup archives). We will securely store your personal information and isolate it from any further processing until deletion is possible.

## 6. What are your privacy rights?

**In Short:** _In some regions, such as the European Economic Area (EEA) and United Kingdom (UK), you have rights that allow greater access to and control your personal information. You may review, change, or terminate your account at any time._

In some regions (like the EEA and UK), you have certain rights under applicable data protection laws. These may include the right:
To request access and obtain a copy of your personal information.
To request rectification or erasure.
To restrict the processing of your personal information.
If applicable, to data portability.
In certain circumstances, you may also have the right to object to the processing of your personal information. To make such a request, please use the contact details provided below. We will consider and act upon any request following applicable data protection laws.

If we are relying on your consent to process your personal information, you have the right to withdraw your consent at any time. However, please note that this will not affect the lawfulness of the processing before its withdrawal, nor will it affect the processing of your personal information conducted in reliance on lawful processing grounds other than consent.

Suppose you are a resident in the EEA or UK, and you believe we are unlawfully processing your personal information. In that case, you also have the right to complain to your local data protection supervisory authority. You can find their contact details here: <https://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm>.

If you are a resident in Switzerland, the contact details for the data protection authorities are available here: <https://www.edoeb.admin.ch/edoeb/en/home.html>.

**Cookies and similar technologies:** Most Web browsers are set to accept cookies by default. If you prefer, you can usually set your browser to remove cookies and reject cookies. If you choose to remove cookies or reject cookies, this could affect certain features or services of our website. For example, to opt-out of interest-based advertising by advertisers on our website, visit <http://www.aboutads.info/choices/>.

## 7. Controls for do-not-track features

Most web browsers and some mobile operating systems and applications include a Do-Not-Track ("DNT") feature or setting you can activate to signal your privacy preference not to have data about your online browsing activities monitored and collected. At this stage, no uniform technology standard for recognizing and implementing DNT signals has been finalized. As such, we do not currently respond to DNT browser signals or any other mechanism that automatically communicates your choice not to be tracked online. If a standard for online tracking is adopted that we must follow in the future, we will inform you about that practice in a revised version of this privacy notice.

## 8. Do California residents have specific privacy rights?

**In Short:** _Yes, if you are a resident of California, you are granted specific rights regarding access to your personal information._

California Civil Code Section 1798.83, also known as the "Shine The Light" law, permits our users who are California residents to request and obtain from us, once a year and free of charge, information about categories of personal information (if any) we disclosed to third parties for direct marketing purposes and the names and addresses of all third parties with which we shared personal information in the immediately preceding calendar year. If you are a California resident and would like to make such a request, please submit your request in writing to us using the contact information provided below.

If you are under 18 years of age, reside in California, and have a registered account with the website, you have the right to request the removal of unwanted data that you publicly post on the website. To request the removal of such data, please contact us using the contact information provided below. Include the email address associated with your account and a statement that you reside in California. We will ensure the data is not publicly displayed on the website, but please be aware that the data may not be completely or comprehensively removed from all our systems (e.g., backups, etc.).

## CCPA Privacy Notice

The California Code of Regulations defines a "resident" as:

- (1) every individual who is in the State of California for other than a temporary or transitory purpose and
- (2) every individual who is domiciled in the State of California who is outside the State of California for a temporary or transitory purpose

All other individuals are defined as "non-residents."

If this definition of "resident" applies to you, we must adhere to certain rights and obligations regarding your personal information.

### What categories of personal information do we collect?

We have collected the following categories of personal information in the past twelve (12) months:

- A. Identifiers: Contact details, such as real name, alias, postal address, telephone or mobile contact number, unique personal identifier, online identifier, Internet Protocol address, email address, and account name.

  NO

- B. Personal information categories listed in the California Customer Records statute: Name, contact information, education, employment, employment history, and financial information.

  YES

- C. Protected classification characteristics under California or federal law: Gender and date of birth.

  NO

- D. Commercial information: Transaction information, purchase history, financial details, and payment information

  NO

- E. Biometric information: Fingerprints and voiceprints

  NO

- F. Internet or other similar network activity: Browsing history, search history, online behavior, interest data, and interactions with our and other websites, applications, systems, and advertisements

  NO

- G. Geolocation data: Device location

  NO

- H. Audio, electronic, visual, thermal, olfactory, or similar information: Images and audio, video or call recordings created in connection with our business activities

  NO

- I. Professional or employment-related information: Business contact details to provide you our services at a business level, job title as well as work history and professional qualifications if you apply for a job with us

  NO

- J. Education Information: Student records and directory information

  NO

- K. Inferences are drawn from other personal information: Inferences are drawn from any of the collected personal information listed above to create a profile or summary about, for example, an individual's preferences and characteristics

  NO

We may also collect other personal information outside of these categories instances where you interact with us in-person, online, or by phone or mail in the context of:

- Receiving help through our customer support channels;

- Participation in customer surveys or contests; and

- Facilitation in the delivery of our Services and to respond to your inquiries.

### How do we use and share your personal information?

More information about our data collection and sharing practices can be found in this privacy notice.

You may contact us by email at brunopalma@gmail.com or by referring to the contact details at the bottom of this document.

If you use an authorized agent to exercise your right to opt-out, we may deny a request if the authorized agent does not submit proof that they have been validly authorized to act on your behalf.

### Will your information be shared with anyone else

We may disclose your personal information with our service providers according to a written contract between each service provider and us. Each service provider is a for-profit entity that processes the information on our behalf.

We may use your personal information for our business purposes, such as internal research for technological development and demonstration. However, this is not considered to be the "selling" of your data.

Marlucio's meow jumps!
Has not disclosed or sold any personal information to third parties for a business or commercial purpose in the preceding 12 months.
Marlucio's meow jumps! Will not sell personal information in the future belonging to website visitors, users, and other consumers.

### Your rights concerning your data

#### Right to request deletion of the data - Request to delete

You can ask for the deletion of your personal information. Suppose you ask us to delete your personal information. In that case, we will respect your request and delete your personal information, subject to certain exceptions provided by law, such as (but not limited to) the exercise by another consumer of his or her right to free speech, our compliance requirements resulting from a legal obligation or any processing that may be required to protect against illegal activities.

### Right to be informed - Request to know

Depending on the circumstances, you have a right to know:

- whether we collect and use your personal information;

- the categories of personal information that we collect;
- the purposes for which the collected personal information is used;
- whether we sell your personal information to third parties;
- the categories of personal data that we traded or disclosed for a business purpose;
- the types of third parties to whom the personal data was sold or disclosed for a business purpose; and
- the business or commercial purpose for collecting or selling personal information.

Under applicable law, we are not obligated to provide or delete consumer information that is de-identified in response to a consumer request or re-identify individual data to verify a consumer request.

Right to Non-Discrimination for the Exercise of a Consumer's Privacy Rights

We will not discriminate against you if you exercise your privacy rights.

### Verification process

Upon receiving your request, we will need to verify your identity to determine you are the same person we have the information with in our system. These verification efforts require us to ask you to provide information to match it with the information you have previously provided us. For instance, depending on the type of request you submit, we may ask you to provide certain information so that we can match the information you provide with the information we already have on file, or we may contact you through a communication method (e.g. phone or email) that you have previously provided to us. We may also use other verification methods as the circumstances dictate.

We will only use the personal information provided in your request to verify your identity or authority to make the request. To the extent possible, we will avoid requesting additional information from you for verification. If, however, we cannot verify your identity from the information already maintained by us, we may request that you provide additional information to verify your identity and for security or fraud prevention purposes. We will delete such additionally provided information as soon as we finish verifying you.

### Other privacy rights

- You may object to the processing of your data.
- You may request correction of your data if it is incorrect or no longer relevant or restrict the data processing.
- You can designate an authorized agent to request the CCPA on your behalf. However, we may deny a request from an authorized agent that does not submit proof that they have been validly authorized to act on your behalf in accordance with the CCPA.

- You may request to opt-out from future selling of your personal information to third parties. Upon receiving a request to opt-out, we will act upon the request as soon as feasibly possible, but no later than 15 days from the request submission date.

To exercise these rights, you can contact us by email at brunopalma@gmail.com or refer to the contact details at the bottom of this document. In addition, if you have a complaint about handling your data, we would like to hear from you.

## 9. Do we make updates to this notice

**In Short:** _Yes, we will update this notice as necessary to stay compliant with relevant laws._

We may update this privacy notice from time to time. The updated version will be indicated by an updated "Revised" date, and the updated version will be effective as soon as it is accessible. In addition, if we make material changes to this privacy notice, we may notify you by prominently posting a notice of such changes or by directly sending you a notification. Therefore, we encourage you to review this privacy notice frequently to be informed of how we are protecting your information.

## 10. How can you contact us about this notice?

If you have questions or comments about this notice, you may email us at brunopalma@gmail.com or by post to:

Marlucio's meow jumps!\
Destouchesstraße 85\
München, Bavaria 80796\
Germany

## 11. How can you review, update, or delete the data we collect from you?

Based on the applicable laws of your country, you may have the right to request access to the personal information we collect from you, change that information, or delete it in some circumstances. To request to review, update, or delete your personal information, please send us an email.
