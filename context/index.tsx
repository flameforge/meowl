import { createContext, ReactChild, ReactFragment, ReactPortal, useState } from 'react'

export interface UserContext {
  username: string
  setUsername: Function
  secret: string
  setSecret: Function
  catsole: string
  setCatsole: Function
  zenmode: boolean
  setZenmode: Function
}

export const Context = createContext(null)

export const ContextProvider = (props: { children: boolean | ReactChild | ReactFragment | ReactPortal }) => {
  const [username, setUsername] = useState('')
  const [secret, setSecret] = useState('')
  const [catsole, setCatsole] = useState('')
  const [zenmode, setZenmode] = useState(false)

  const value: UserContext = {
    username,
    setUsername,
    secret,
    setSecret,
    catsole,
    setCatsole,
    zenmode,
    setZenmode,
  }

  return <Context.Provider value={value}>{props.children}</Context.Provider>
}
