const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

module.exports = withBundleAnalyzer({
  webpack: (config) => {
    config.module.rules.push({
      test: /\.(txt|md)$/,
      use: 'raw-loader',
    });

    return config;
  },
  reactStrictMode: true,
});
