import isEncodedURI from './isEncodedURI'

const decodeURI = (uri: string) => {
  while (isEncodedURI(uri)) {
    uri = decodeURIComponent(uri)
  }

  return uri
}

export default decodeURI
