import { lowerCaseEverything, removeSpacesAtStartAndEnd } from './strings'

function denoise(message: string) {
  return lowerCaseEverything(removeSpacesAtStartAndEnd(message))
}

export default denoise
