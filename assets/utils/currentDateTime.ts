const currentDateTime = (now = new Date()) =>
  now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds() + ' @ ' + now.getDate() + '/' + (now.getMonth() + 1) + '/' + now.getFullYear()

export default currentDateTime
