export const removeSpacesAtStartAndEnd = (message: string) => message?.trim()
export const lowerCaseEverything = (message: string) => message?.toLowerCase()
