function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText)
  }
  return response
}

function fetchData(url: string) {
  const apiUrl = process.env.NEXT_PUBLIC_API_URL
  const data = fetch(`${apiUrl}${url}`)
    .then(handleErrors)
    .then((res) => res.json())
  return data
}

export default fetchData
