export default function isEncodedURI(uri = '') {
  return uri !== decodeURIComponent(uri)
}
