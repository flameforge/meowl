import { lowerCaseEverything, removeSpacesAtStartAndEnd } from './strings'

const alreadyThere = (path: string) => {
  const here = window.location.href
  const there = lowerCaseEverything(removeSpacesAtStartAndEnd(path))
  const areWeThereYet = there === here
  return areWeThereYet
}

export default alreadyThere
