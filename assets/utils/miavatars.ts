const miavatars = (i: number) => {
  const knownMiavatars = 10
  const x = (i + knownMiavatars) % knownMiavatars
  return `art/miavatars/meow${x}.png`
}

export default miavatars
