import { amber, red } from '@mui/material/colors'
import { createTheme, responsiveFontSizes } from '@mui/material/styles'
import fonts from './fonts'

let theme = createTheme({
  palette: {
    primary: red,
    secondary: amber,
  },
  typography: {
    fontFamily: fonts.join(','),
  },
})

theme = responsiveFontSizes(theme)

export default theme
