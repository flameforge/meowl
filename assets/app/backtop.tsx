import ArrowUpward from '@mui/icons-material/ArrowUpward'
import { Fab } from '@mui/material'
import useMediaQuery from '@mui/material/useMediaQuery'
import React, { useEffect, useState } from 'react'

export const scrollToTop = () =>
  window.scrollTo({
    top: 0,
    behavior: 'smooth',
  })

export default function BackTop(): JSX.Element {
  const [showButton, setShowButton] = useState(false)
  const matchesSm = useMediaQuery('(max-width:600px)')
  const matchesMd = useMediaQuery('(max-width:900px)')

  useEffect(() => window.addEventListener('scroll', () => setShowButton(window.pageYOffset > 300)), [])

  return (
    <Fab
      aria-label="Back to the top of the page"
      component="nav"
      onClick={scrollToTop}
      color="primary"
      size={matchesSm ? 'small' : matchesMd ? 'medium' : 'large'}
      sx={{
        position: 'fixed',
        bottom: 0,
        right: 0,
        mr: [1.75, 2, 2.5, 4, 30],
        mb: [1.75, 2, 2, 2.5, 3],
        opacity: showButton ? 1 : 0,
        transition: 'opacity .2s ease',
      }}
    >
      <ArrowUpward />
    </Fab>
  )
}
