import React from 'react'

declare interface MeowSpace {
  inputValue: string
}

const meowSpace: MeowSpace = {
  inputValue: '',
}

const AppContext = React.createContext(meowSpace)

export default AppContext
