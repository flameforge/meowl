import MenuIcon from '@mui/icons-material/Menu'
import { Button } from '@mui/material'
import AppBar from '@mui/material/AppBar'
import IconButton from '@mui/material/IconButton'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import Link from 'next/link'
import * as React from 'react'
import { Context } from '../../context'

const styles = {
  container: { flexGrow: 1 },
  appBar: { background: 'transparent', color: 'rgba(0, 0, 0, 0.87)' },
  logo: { flexGrow: 1, ml: 1, mr: 1, textDecoration: 'none', cursor: 'pointer', display: 'inline-block' },
  account: { marginLeft: 'auto' },
}

export default function MenuAppBar(): JSX.Element {
  const { catsole } = React.useContext(Context)
  const [zen, setZen] = React.useState(false)

  React.useEffect(() => {
    setZen(catsole.length > 0)
  }, [catsole])

  return (
    <AppBar
      position="static"
      elevation={0}
      style={styles.appBar}
      component="nav"
      data-testid="main-navigation"
      sx={{ opacity: zen ? 0 : 1, transition: 'opacity .2s ease' }}
    >
      <Toolbar>
        <Link passHref href="/directory">
          <IconButton size="large" edge="start" color="inherit" aria-label="Sitemap page" sx={{ color: 'rgb(244, 67, 54)' }}>
            <MenuIcon sx={{ color: 'rgb(244, 67, 54)' }} />
          </IconButton>
        </Link>

        <Link passHref href="/" aria-label="Home page">
          <Button style={{ textTransform: 'initial', backgroundColor: 'transparent' }}>
            <Typography variant="h6" component="div" sx={styles.logo} color="text.primary">
              Meowl
            </Typography>
          </Button>
        </Link>

        {/* <IconButton size="large" aria-label="account of current user" color="inherit" sx={styles.account}>
          <AccountCircle />
        </IconButton> */}
      </Toolbar>
    </AppBar>
  )
}
