import { Container, Link as MuiLink, Typography } from '@mui/material'
import Link from 'next/link'
import React, { useContext, useEffect, useState } from 'react'
import { Context } from '../../context'

const styles = {
  copyleft: { display: 'inline-block', transform: 'scale(-1, 1)', mr: 0.5, ml: 0.5 },
  copyright: { mr: 0.5, ml: 0.5 },
  link: { mr: 0.5, ml: 0.5, opacity: 0.75, '&:hover': { opacity: 1 } },
}

export default function Footerbar(): JSX.Element {
  const { catsole } = useContext(Context)
  const [zen, setZen] = useState(false)

  useEffect(() => {
    setZen(catsole.length > 0)
  }, [catsole])

  return (
    <Container component="footer" sx={{ p: 2, opacity: zen ? 0 : 1, transition: 'opacity .2s ease' }}>
      <Typography variant="caption" color="primary" sx={styles.copyleft}>
        ©
      </Typography>

      <Typography variant="caption" color="text.secondary" sx={styles.copyright}>
        {new Date().getFullYear()} Marlucio&apos;s meow jumps!
      </Typography>

      <Link passHref href="/about" aria-label="About page">
        <MuiLink component="a" variant="caption" sx={styles.link} color="text.primary">
          What is this?
        </MuiLink>
      </Link>

      <Link passHref href="/privacy" aria-label="Privacy Policy page">
        <MuiLink component="a" variant="caption" sx={styles.link} color="text.primary">
          Privacy Policy
        </MuiLink>
      </Link>

      <Link passHref href="/terms" aria-label="Terms of Use page">
        <MuiLink component="a" variant="caption" sx={styles.link} color="text.primary">
          Terms of Use
        </MuiLink>
      </Link>

      <Link passHref href="mailto:flameforge@gmail.com" aria-label="Send email to support">
        <MuiLink component="a" variant="caption" sx={styles.link} color="text.primary">
          Contact
        </MuiLink>
      </Link>
    </Container>
  )
}
