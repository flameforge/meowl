import Bowser from 'bowser'
import currentDateTime from '../utils/currentDateTime'
import { isDev } from '../utils/isDev'
import { story } from './story'

export function initialize() {
  const environment = isDev ? 'Development' : 'Production'
  const { vendor, platform, language, languages, cookieEnabled, doNotTrack } = window.navigator
  const browser = Bowser.getParser(window.navigator.userAgent)
  const name = browser.getBrowser().name
  const version = browser.getBrowser().version
  const { href, hostname, pathname, protocol } = window.location
  const screenHeight = window.screen.height
  const screenWidth = window.screen.width

  // Storyline
  console.log('%c' + currentDateTime(new Date()) + ' - ' + environment, 'color: dimgray')

  if (!isDev) story.intro()

  // prettier-ignore
  story.cat.walksIn(
      href, hostname, pathname, protocol, name, version, vendor, platform, language, languages.join(', '),
      cookieEnabled, doNotTrack, screenWidth, screenHeight
    );
}
