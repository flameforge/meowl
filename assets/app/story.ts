import { ascii } from '../../data/asciis'

const tell = (story: string) => console.log(story)

export const story = {
  intro: () => tell(`\n${ascii('meowjumps')}\n`),
  cat: {
    walksIn: (
      href,
      hostname,
      pathname,
      protocol,
      name,
      version,
      vendor,
      platform,
      language,
      languages,
      cookieEnabled,
      doNotTrack,
      screenWidth,
      screenHeight
    ) =>
      tell(`\n🐈  A cat just walked in\n\n
    Agent       Browser:    ${name} ${version} from ${vendor}
                Platform:   ${platform}
                Language:   ${language} selected, also speaks ${languages}
                Cookies:    ${cookieEnabled ? 'Enabled' : 'Disabled'}
                Tracking:   ${doNotTrack === '0' ? 'Enabled' : 'Disabled'}\n
    Location    href:       ${href}
                hostname:   ${hostname}
                pathname:   ${pathname}
                protocol:   ${protocol}\n
    Device      width:      ${screenWidth}px
                height:     ${screenHeight}px\n\n`),
    meows: (phrase) => tell(`\n    ${ascii('meowr')}\n\n   ${phrase}\n\n`),
    reentersTheRoom: () => tell(`\n${ascii('catleaves')}\n\nCat leaves the room for pure f5ness \n\n`),
    jumps: (target) => tell(`\n${ascii('catjump')}\n\nCat Jumps to ${target}\n\n`),
    retreats: () => tell(`\n${ascii('turnsback')}\n\nCat goes back to where he was as soon as the door is open\n\n`),
    retreatsAgain: () => tell(`\n${ascii('catjump')}\n\nCat goes back to where he was as soon as the door is open\n\n`),
  },
  human: {
    hearsTheMeow: (phrase) => tell(`\n  🧏  I heard something, the cat meowed ${phrase}\n\n`),
    saysDontUnderstand: () => tell(`\n  🤷  No clue what that meow means, fallback\n\n`),
    saysDefaultAction: () => tell(`\n  💁  I'll just open the main door\n\n`),
    saysAlreadyHere: () => tell(`\n  🧍  We are already here\n\n`),
    saysToEmptyMeow: (phrase) => tell(`\n  🧍  ${phrase}\n\n`),
    turnsLightsOffAndOn: () => tell(`\n🚪🚶  Gets up from chair and turns the lights off\n\n`),
    opensPage: (page) => tell(`\n🚪🚶  Gets up from chair and Opens the door to\n\n${page} page\n\n`),
    opensDoorToPreviousRoom: () => tell(`\n🚪🚶  Gets up from chair and Opens the door to the previous room\n\n`),
    opensDoorToPreviousRoomAgain: () => tell(`\n🚪🚶  Gets up from chair and Opens the door to the previous room\n\n`),
    opensDoorToOutside: (title, url, prefix, phrase, suffix) =>
      tell(`\n🚪🚶  Gets up from chair and Opens the door to the outside world 🌎 🌃🌆🛣️\n\n\nThe Door leads to ${title} -> "${
        phrase || 'no search query'
      }"\n
    url:        ${url}
    prefix:     ${prefix}
    suffix:     ${suffix}\n\n`),
  },
  thinking: {
    decodesTheMessage: (phrase, message) => tell(`\n  🧠  Decoding ${phrase} into ${message}\n\n`),
    denoisesTheMessage: (phrase, message) => tell(`\n  🧠  Denoising ${phrase} into ${message}\n\n`),
  },
}
