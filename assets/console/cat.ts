import Router from 'next/router'
import { story } from '../app/story'

export function jump(url: string) {
  story.cat.jumps(url)

  Router.push(url)
}

export function retreat() {
  story.cat.retreats()
  window.history.back()
}

export function retreatAgain() {
  story.cat.retreatsAgain()
  window.history.forward()
}

export function reenterTheRoom() {
  story.cat.reentersTheRoom()
  location.reload()
}
