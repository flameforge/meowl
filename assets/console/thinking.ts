import { Meow, meowsFromPortalName, meowsWithName, meowsWithPhrase } from '../../data/meows'
import { Portal } from '../../data/portals'
import denoise from '../utils/denoise'
import decodeURI from '../utils/fullyDecodeURI'
import isEncodedURI from '../utils/isEncodedURI'
import { fallBack, goToPortal } from './human'

export const act = (meow: Meow) => meow.command()

export function Think(phrase: string) {
  let message = phrase

  message = decode(message)
  message = denoise(message)

  const meows = [...meowsWithPhrase(message), ...meowsWithName(message), ...meowsFromPortalName(message)]

  const moreThanOne = meows.length > 1
  const onlyOne = meows.length === 1

  const firstMeow = meows[0]

  if (moreThanOne) {
    act(firstMeow)
    return
  }

  if (onlyOne) {
    act(firstMeow)
    return
  }

  fallBack(phrase)
}

export const decode = (message: string) => {
  let x = message
  const encoded = isEncodedURI(x)
  if (encoded) x = decodeURI(x)
  return x
}

export const EmptyMeowAnswers = [
  "I can hear you meow, but you didn't say anything",
  'I heard you, yes, what do you want?',
  'Meow, meow, meow...',
  'Meow!!!',
  'Type help and press enter',
  'Type "help" and then press the key "Enter"',
  'Ok, just h, type "h" and press Enter',
]

export const convertPortalToMeow = (portal: Portal): Meow => {
  return {
    name: portal.name,
    description: portal.description,
    phrases: [portal.name],
    avatar: portal.icon,
    command: () => goToPortal({ ...portal, searcheable: false }),
  }
}
