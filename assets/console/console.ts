/* eslint-disable @typescript-eslint/no-explicit-any */
import { MutableRefObject } from 'react'

export const clearConsole = () => console?.clear()
export const focusInput = (x: MutableRefObject<any>) => x.current.focus()
export const selectText = (x: MutableRefObject<any>) => x.current.select()
