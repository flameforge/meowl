/* eslint-disable @typescript-eslint/no-explicit-any */
import SearchIcon from '@mui/icons-material/Search'
import { debounce, IconButton, InputAdornment, TextField } from '@mui/material'
import Autocomplete from '@mui/material/Autocomplete'
import match from 'autosuggest-highlight/match'
import parse from 'autosuggest-highlight/parse'
import React, { MutableRefObject, useCallback, useContext, useEffect, useRef, useState } from 'react'
import { Context } from '../../context'
import { getOptionsAsync } from '../../data/suggestions'
import { story } from '../app/story'
import { focusInput, selectText } from './console'
import { EmptyMeowAnswers, Think } from './thinking'

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default function Meowing(props): JSX.Element {
  const textInput: MutableRefObject<any> = useRef(null)
  const [placeHolder, setPlaceholder] = useState('Type a command or help')
  const [emptyMeowCount, setEmptyMeowCount] = useState(0)
  const [options, setOptions] = useState([])

  const { catsole, setCatsole } = useContext(Context)

  useEffect(() => {
    focusInput(textInput)
  }, [])

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const getOptionsDelayed = useCallback(
    debounce((text, callback) => {
      setOptions([])
      getOptionsAsync(text).then(callback)
    }, 0),
    []
  )

  useEffect(() => {
    getOptionsDelayed(catsole, (filteredOptions: React.SetStateAction<any[]>) => {
      setOptions(filteredOptions)
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [catsole])

  function meowed(phrase = catsole) {
    story.cat.meows(phrase)

    heardTheMeow()
    selectText(textInput)

    function heardTheMeow() {
      story.human.hearsTheMeow(phrase)

      const emptyMeow = !phrase
      if (emptyMeow) replyToEmptyMeow()
      else {
        Think(phrase)
      }
    }
  }

  function replyToEmptyMeow() {
    const answer = EmptyMeowAnswers[emptyMeowCount]
    story.human.saysToEmptyMeow(answer)

    setPlaceholder(answer)

    const gaveAllMyAnswers = emptyMeowCount >= EmptyMeowAnswers.length - 1
    if (gaveAllMyAnswers) setEmptyMeowCount(0)
    else setEmptyMeowCount(emptyMeowCount + 1)
  }

  return (
    <Autocomplete
      freeSolo
      options={options}
      autoSelect
      filterOptions={(x) => x} // disable filtering on client
      onInputChange={(e, value) => setCatsole(value)}
      getOptionLabel={(x) => x.name || catsole}
      onKeyPress={(e) => {
        if (e.key === 'Enter') meowed()
      }}
      disableClearable
      renderInput={(params) => (
        <TextField
          {...params}
          fullWidth
          margin="normal"
          label="Console"
          placeholder={placeHolder}
          inputRef={textInput}
          InputProps={{
            ...params.InputProps,
            spellCheck: 'false',
            autoCapitalize: 'none',
            autoComplete: 'off',
            endAdornment: (
              <InputAdornment position="end">
                <IconButton edge="end" color="primary" onClick={() => meowed()} aria-label="Search" sx={{ marginLeft: '-35px' }}>
                  <SearchIcon />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      )}
      renderOption={(props, option, { inputValue }) => {
        const matches = match(option.name, inputValue)
        const parts = parse(option.name, matches)

        return (
          <li {...props}>
            <div onClick={() => meowed(option.name)}>
              {parts.map((part, index) => (
                <span
                  key={index}
                  style={{
                    fontWeight: part.highlight ? 700 : 400,
                  }}
                >
                  {part.text}
                </span>
              ))}
            </div>
          </li>
        )
      }}
    />
  )
}
