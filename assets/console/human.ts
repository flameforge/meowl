import { publicPortals } from '../../data/public/portals'
import { room } from '../../data/rooms'
import { story } from '../app/story'
import alreadyThere from '../utils/alreadyHere'
import { jump, reenterTheRoom, retreat } from './cat'

export function fallBack(phrase: string) {
  story.human.saysDontUnderstand()
  defaultAction(phrase)
}

export function defaultAction(phrase: string) {
  story.human.saysDefaultAction()
  goToPortal(undefined, phrase)
}

export function goToPage(name: string) {
  const { path } = room(name)
  const here = location.protocol + '//' + location.host + path
  if (alreadyThere(here)) story.human.saysAlreadyHere()
  else {
    story.human.opensPage(name)
    jump(path)
  }
}

export function goToPortal({ searcheable, name, url, prefix = '', suffix = '' } = publicPortals[0], phrase?: string) {
  story.human.opensDoorToOutside(name, url, prefix, phrase, suffix)
  let path = url
  if (searcheable && phrase?.length > 0) {
    path += prefix + phrase + suffix
  }
  jump(path)
}

export function turnTheLightsOffAndOn() {
  story.human.turnsLightsOffAndOn()
  reenterTheRoom()
}

export function openDoorToPreviousRoom() {
  story.human.opensDoorToPreviousRoom()
  retreat()
}

export function openDoorToPreviousRoomAgain() {
  story.human.opensDoorToPreviousRoomAgain()
  retreat()
}
