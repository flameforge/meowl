module.exports = {
  printWidth: 155,
  singleQuote: true,
  useTabs: false,
  tabWidth: 2,
  semi: false,
  bracketSpacing: true,
}
