import { CardActionArea } from '@mui/material';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import React, { useState } from 'react';
import { youtubeImgApi } from '../assets/app/external';
import Meowing from '../assets/console/meowing';

function Gold({ nuggets }): JSX.Element {
  const [phrase, setPhrase] = useState('');
  // const [filteredNuggets, setFilteredNuggets] = useState([]);

  // useEffect(() => {
  //   setFilteredNuggets(allNuggets);
  // }, []);

  // useEffect(() => {
  //   if (phrase.length > 0) updateFilters();
  //   else setFilteredNuggets(nuggets);

  //   function updateFilters() {
  //     const matchingTitle = filterNuggetsByTitle(phrase);
  //     const matchingAuthor = filterNuggetsByAuthor(phrase);
  //     const matchingDescription = filterNuggetsByDescription(phrase);
  //     const allMatches = [...new Set([...matchingTitle, ...matchingAuthor, ...matchingDescription])];
  //     setFilteredNuggets(allMatches);
  //   }
  // }, [phrase]);

  return (
    <>
      <Container component="header" maxWidth="md" sx={{ pt: 7 }}>
        <Typography variant="h1" align="center" gutterBottom>
          cat gold
        </Typography>

        <Typography variant="subtitle1" align="center" gutterBottom>
          In fact, according to Bloomberg&quot;s The Magazine Trying to Bring the Web&quot;s Cat Obsession Offline, cats drive almost 15 percent of all Web
          traffic. There are also about 30 million Google searches per month for the search term &quot;cat&quot;.
        </Typography>
      </Container>

      <Container component="section" maxWidth="md">
        <Meowing setPhrase={setPhrase} />
      </Container>

      <Container maxWidth="lg" sx={{ mb: 3, mt: 4 }}>
        <Grid container spacing={1}>
          {nuggets.map((nugget) => (
            <Grid item key={nugget.title} xs={6} sm={4} md={3}>
              <Card sx={{ maxWidth: 345 }}>
                <CardActionArea href={nugget.url} target="_blank" tabIndex={-1}>
                  <CardMedia
                    component="img"
                    alt={nugget.description}
                    image={nugget.type === 'video' ? youtubeImgApi + nugget.vid + '/0.jpg' : nugget.url}
                    height="140"
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      {nugget.title}
                    </Typography>
                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                      {nugget.author}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      {nugget.description}
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button size="small">{nugget.button || 'Jump to'}</Button>
                  </CardActions>
                </CardActionArea>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>
    </>
  );
}

Gold.getInitialProps = async () => {
  const res = await fetch('http://localhost:3000/api/gold/nuggets');
  const json = await res.json();
  return { nuggets: json };
};

export default Gold;
