import {
  Avatar,
  Box,
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardHeader,
  Container,
  Grid,
  LinearProgress,
  Stack,
  Typography,
} from '@mui/material'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import React, { Fragment, useEffect } from 'react'
import { useInView } from 'react-intersection-observer'
import { useInfiniteQuery } from 'react-query'
import { useUIDSeed } from 'react-uid'
import { jump } from '../assets/console/cat'
import Meowing from '../assets/console/meowing'
import fetchData from '../assets/utils/fetchData'
import { Context } from '../context'
import { Portal } from '../data/portals'

const fetchPortals = async ({ pageParam = 1 }) => {
  const url = `/api/portals/?page=${pageParam}`
  const portals = await fetchData(url)
  return portals
}

export const getServerSideProps: GetServerSideProps = async () => {
  const data = await fetchData('/api/portals')

  const portals = {
    pages: [{ data }],
    pageParams: [null],
  }

  return {
    props: {
      portals: portals,
    },
  }
}

export default function Portals(): JSX.Element {
  const seed = useUIDSeed()

  const { catsole } = React.useContext(Context)
  const [zen, setZen] = React.useState(false)

  useEffect(() => {
    setZen(catsole.length > 0)
  }, [catsole])

  const { data, isSuccess, fetchNextPage, hasNextPage, isLoading } = useInfiniteQuery('portals', fetchPortals, {
    getNextPageParam: (stack) => (stack.page === stack.pages ? undefined : stack.page + 1),
    refetchOnWindowFocus: false,
  })

  const [ref, inView] = useInView({
    threshold: 0,
  })

  useEffect(() => {
    if (hasNextPage) fetchNextPage()
  }, [fetchNextPage, hasNextPage, inView])

  return (
    <>
      <Head>
        <title>Meowl space</title>
        <meta name="description" content="Simple homepage to jump into the service that you want" />
      </Head>

      <Container component="header" maxWidth="md" sx={{ pt: 7 }}>
        <Typography variant="h1" align="center" gutterBottom>
          red dot gallery
        </Typography>

        <Typography variant="subtitle1" align="center" gutterBottom sx={{ opacity: zen ? 0 : 1, transition: 'opacity .2s ease' }}>
          All the search engines and memorable places of the internet for you to jump to.
        </Typography>
      </Container>

      <Container component="section" maxWidth="md">
        <Meowing />
      </Container>

      <Container component="main" sx={{ pt: 3, pb: 6 }}>
        <Grid container rowSpacing={1} spacing={1}>
          {isSuccess &&
            data?.pages.map((page) => (
              <Fragment key={seed(page)}>
                {page.payload.map(({ name, icon, description, url, prefix, suffix }: Portal, index) => (
                  <Grid item key={name} xs={6} sm={4} md={3}>
                    <Card key={index} sx={{ boxShadow: 1, mb: 2, maxWidth: 345 }} component="article">
                      <CardActionArea tabIndex={-1} onClick={() => jump(url)}>
                        <CardHeader
                          avatar={<Avatar alt={name} src={icon} />}
                          title={<strong>{name}</strong>}
                          subheader={url}
                          sx={{ background: 'whitesmoke' }}
                        />
                        <CardContent>
                          <Typography variant="body2">{description}</Typography>

                          <Stack spacing={0.5} component="dl" justifyContent="left" alignItems="left" sx={{ mb: 0 }}>
                            {prefix && (
                              <Box>
                                <Typography variant="caption" color="text.secondary" component="dt" sx={{ float: 'left', mr: 1 }}>
                                  Prefix:
                                </Typography>

                                <Typography variant="caption" color="text.secondary" component="dd">
                                  {prefix}
                                </Typography>
                              </Box>
                            )}

                            {suffix && (
                              <Box>
                                <Typography variant="caption" color="text.secondary" component="dt" sx={{ float: 'left', mr: 1 }}>
                                  Suffix:
                                </Typography>

                                <Typography variant="caption" color="text.secondary" component="dd">
                                  {suffix}
                                </Typography>
                              </Box>
                            )}
                          </Stack>
                        </CardContent>
                        <CardActions>
                          <Button component="span">Visit</Button>
                        </CardActions>
                      </CardActionArea>
                    </Card>
                  </Grid>
                ))}
              </Fragment>
            ))}
        </Grid>

        <Box ref={ref} sx={{ mt: 3, mb: 3 }}>
          {isLoading ? <LinearProgress /> : ''}
        </Box>
      </Container>
    </>
  )
}
