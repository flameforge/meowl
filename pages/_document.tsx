import Document, { Head, Html, Main, NextScript } from 'next/document'
import * as React from 'react'
import theme from '../assets/visuals/theme'

class MyDocument extends Document {
  render() {
    return (
      <Html lang="en">
        <Head>
          <meta name="theme-color" content={theme.palette.primary.main} />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
