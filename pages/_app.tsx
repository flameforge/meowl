import { CacheProvider } from '@emotion/react'
import CssBaseline from '@mui/material/CssBaseline'
import { ThemeProvider } from '@mui/material/styles'
import type { AppProps } from 'next/app'
import Head from 'next/head'
import Script from 'next/script'
import * as React from 'react'
import Favicon from 'react-favicon'
import { QueryClient, QueryClientProvider } from 'react-query'
import MenuAppBar from '../assets/app/appbar'
import BackTop from '../assets/app/backtop'
import FooterBar from '../assets/app/footerbar'
import { initialize } from '../assets/app/initialize'
import createEmotionCache from '../assets/utils/createEmotionCache'
import nyan from '../assets/visuals/nyan'
import '../assets/visuals/styles.scss'
import theme from '../assets/visuals/theme'
import { ContextProvider } from '../context'

const clientSideEmotionCache = createEmotionCache()
const queryClient = new QueryClient()

export default function MyApp({ Component, pageProps }: AppProps) {
  const { emotionCache = clientSideEmotionCache } = pageProps

  React.useEffect(() => {
    initialize()
  }, [])

  return (
    <>
      <Script strategy="lazyOnload" src={`https://www.googletagmanager.com/gtag/js?id=${process.env.NEXT_PUBLIC_GA_ID}`} />
      <Script id="ga-analytics">
        {`
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', '${process.env.NEXT_PUBLIC_GA_ID}');
          `}
      </Script>

      <CacheProvider value={emotionCache}>
        <Head>
          <title>Meowl space</title>
          <meta name="viewport" content="initial-scale=1, width=device-width" />
        </Head>

        <Favicon url={nyan} />

        <ThemeProvider theme={theme}>
          <ContextProvider>
            <QueryClientProvider client={queryClient}>
              <MenuAppBar />
              <CssBaseline />

              <Component {...pageProps} />

              <FooterBar />
              <BackTop />
            </QueryClientProvider>
          </ContextProvider>
        </ThemeProvider>
      </CacheProvider>
    </>
  )
}
