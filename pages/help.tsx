import { Box, Container, Typography } from '@mui/material'
import Head from 'next/head'
import Image from 'next/image'
import React, { useEffect } from 'react'
import Meowing from '../assets/console/meowing'
import { Context } from '../context'
import obicat from '../public/art/obicat.jpeg'

export default function Help(): JSX.Element {
  const { catsole } = React.useContext(Context)
  const [zen, setZen] = React.useState(false)

  useEffect(() => {
    setZen(catsole.length > 0)
  }, [catsole])

  return (
    <>
      <Head>
        <title>Meowl space</title>
        <meta name="description" content="Simple homepage to jump into the service that you want" />
      </Head>

      <Container component="header" maxWidth="md" sx={{ pt: 7 }}>
        <Typography variant="h1" align="center" gutterBottom>
          help me
        </Typography>

        <Typography variant="subtitle1" align="center" gutterBottom sx={{ opacity: zen ? 0 : 1, transition: 'opacity .2s ease' }}>
          Type &quot;meows&quot; to see the list of available commands.
        </Typography>
      </Container>

      <Container component="section" maxWidth="md">
        <Meowing />
      </Container>

      <Box component="main" display="flex" justifyContent="center" sx={{ opacity: zen ? 0 : 1, transition: 'opacity .2s ease' }}>
        <figure>
          <Image src={obicat} alt="Obi-cat Kenobi" />
          <figcaption>
            <Typography variant="caption">Help me Obi-cat Kenobi, you are my only hope!</Typography>
          </figcaption>
        </figure>
      </Box>
    </>
  )
}
