import CalendarTodayOutlinedIcon from '@mui/icons-material/CalendarTodayOutlined'
import FlagOutlinedIcon from '@mui/icons-material/FlagOutlined'
import LocationOnOutlinedIcon from '@mui/icons-material/LocationOnOutlined'
import PersonOutlineOutlinedIcon from '@mui/icons-material/PersonOutlineOutlined'
import { Card, CardActionArea, CardContent, CardMedia, Container, Grid, Typography } from '@mui/material'
import Button from '@mui/material/Button'
import CardActions from '@mui/material/CardActions'
import React, { useEffect, useState } from 'react'
import styles from '../assets/visuals/runaways.module.scss'

export default function Runaways(): JSX.Element {
  const [data, setData] = useState(null)
  const [isLoading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    fetch('https://dev.fluur.org/example.json')
      .then((res) => res.json())
      .then((data) => {
        console.log('🚀 ~ file: runaways.tsx ~ line 13 ~ .then ~ data', data)
        setData(data)
        setLoading(false)
      })

    // injecting fonts
    const fonts = document.createElement('style')
    fonts.appendChild(
      document.createTextNode("@import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;700&family=Work+Sans&display=swap');")
    )
    document.head.appendChild(fonts)
  }, [])

  if (isLoading) return <p>Loading...</p>
  if (!data) return <p>No profile data</p>

  document.body.style.backgroundColor = '#FFE8B5'
  return (
    <>
      <Container component="header" maxWidth="md" sx={{ pt: 3 }}>
        <Typography
          variant="h3"
          align="center"
          gutterBottom
          component="h1"
          sx={{
            fontFamily: "'Montserrat', sans-serif;",
            fontWeight: '700;',
            color: '#3B3735',
            padding: '0 2rem',
          }}
        >
          Explore Destinations & Activities
        </Typography>
      </Container>
      <Container sx={{ mb: 3, mt: 2, maxWidth: '1341px!important' }} component="main">
        <Grid container spacing={3}>
          {data.map((item, index) => (
            <Grid item key={item.name} xs={12} md={6} lg={4}>
              <Card key={index} sx={{ maxWidth: 345, color: 'white', margin: 'auto' }} component="article" className={styles.card}>
                <CardActionArea href={item.url} target="_blank" tabIndex={-1} rel="noreferrer">
                  <figure className={styles.figure}>
                    <CardMedia className={styles.img} component="img" alt={item.name} image="https://placebear.com/640/360" height="240" loading="lazy" />
                  </figure>

                  <Typography gutterBottom className={styles.price} component="aside">
                    $ {item.price} / night
                  </Typography>

                  <CardContent
                    component="main"
                    sx={{
                      pt: 0,
                      pb: 0,
                      pl: 2.75,
                      pr: 2.75,
                      flexGrow: 1,
                    }}
                  >
                    <Typography
                      gutterBottom
                      variant="body1"
                      component="h2"
                      className={styles.name}
                      sx={{
                        fontFamily: "'Montserrat', sans-serif;",
                        fontWeight: '700;',
                        textAlign: 'center',
                        fontSize: '20px',
                        // lineHeight: '2.8rem',
                        color: '#3B3735',
                        mb: 0.875,
                        mt: 1.5,
                      }}
                    >
                      {item.name}
                    </Typography>

                    <Typography
                      className={styles.summary}
                      gutterBottom
                      sx={{
                        color: '#3B3735',
                        fontFamily: "'Work Sans', sans-serif;",
                        fontSize: '18px',
                        // lineHeight: '26.32px',
                        textAlign: 'center',
                      }}
                    >
                      {item.summary}
                    </Typography>
                    <div className={styles.tokens}>
                      <Typography gutterBottom variant="body1" className={styles.token}>
                        <LocationOnOutlinedIcon /> {item.startLocation.description}
                      </Typography>

                      <Typography gutterBottom variant="body1" className={styles.token}>
                        <CalendarTodayOutlinedIcon /> June 2021
                      </Typography>

                      <Typography gutterBottom variant="body1" className={styles.token}>
                        <FlagOutlinedIcon /> {item.locations.length} stops
                      </Typography>

                      <Typography gutterBottom variant="body1" className={styles.token}>
                        <PersonOutlineOutlinedIcon /> {item.maxGroupSize} People
                      </Typography>
                    </div>
                  </CardContent>

                  <CardActions className={styles.actions}>
                    <Typography gutterBottom className={styles.ratings} id="ratings">
                      Rating: {item.ratingsAverage} ({item.ratingsQuantity})
                    </Typography>

                    <Button variant="contained" disableElevation className={styles.details} component="div">
                      Details
                    </Button>
                  </CardActions>
                </CardActionArea>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>{' '}
    </>
  )
}
