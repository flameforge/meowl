import { Box, Container, Typography } from '@mui/material'
import Head from 'next/head'
import Image from 'next/image'
import React, { useEffect } from 'react'
import Meowing from '../assets/console/meowing'
import { Context } from '../context'
import suicidecat from '../public/art/suicidecat.gif'

export default function About(): JSX.Element {
  const { catsole } = React.useContext(Context)
  const [zen, setZen] = React.useState(false)

  useEffect(() => {
    setZen(catsole.length > 0)
  }, [catsole])

  return (
    <>
      <Head>
        <title>Meowl space</title>
        <meta name="description" content="Simple homepage to jump into the service that you want" />
      </Head>

      <Container component="header" maxWidth="md" sx={{ pt: 7 }}>
        <Typography variant="h1" align="center" gutterBottom>
          about
        </Typography>

        <Typography variant="subtitle1" align="center" gutterBottom sx={{ opacity: zen ? 0 : 1, transition: 'opacity .2s ease' }}>
          Cats and especially kittens can be very playful and energetic. Sometimes they expel all that energy by darting from room to room, jumping up and
          down on the furniture, and going a little crazy.
        </Typography>
      </Container>

      <Container component="section" maxWidth="md">
        <Meowing />
      </Container>

      <Box display="flex" justifyContent="center" component="main" sx={{ opacity: zen ? 0 : 1, transition: 'opacity .2s ease' }}>
        <figure>
          <Image src={suicidecat} alt="Obi-cat Kenobi" />
          <figcaption>
            <Typography variant="caption">I&apos;m a cat, I go meow meow meow </Typography>
          </figcaption>
        </figure>
      </Box>
    </>
  )
}
