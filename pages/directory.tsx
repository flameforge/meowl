import { Container, List, ListItem, Typography } from '@mui/material'
import Head from 'next/head'
import Link from 'next/link'
import React, { useEffect } from 'react'
import Meowing from '../assets/console/meowing'
import { Context } from '../context'

export default function Directory(): JSX.Element {
  const { catsole } = React.useContext(Context)
  const [zen, setZen] = React.useState(false)

  useEffect(() => {
    setZen(catsole.length > 0)
  }, [catsole])

  return (
    <>
      <Head>
        <title>Meowl space</title>
        <meta name="description" content="Simple homepage to jump into the service that you want" />
      </Head>

      <Container component="header" maxWidth="md" sx={{ pt: 7 }}>
        <Typography variant="h1" align="center" gutterBottom>
          directory
        </Typography>

        <Typography align="center" gutterBottom sx={{ opacity: zen ? 0 : 1, transition: 'opacity .2s ease' }}>
          These are the pages that you will find inside meowl.
        </Typography>
      </Container>

      <Container component="nav" maxWidth="md">
        <Meowing />
      </Container>

      <Container component="main" maxWidth="md" sx={{ pt: 3, pb: 3 }}>
        <List>
          <Link passHref href="/" aria-label="Home page">
            <ListItem button component="a">
              home
            </ListItem>
          </Link>

          <Link passHref href="/commands" aria-label="Commands page">
            <ListItem button component="a">
              commands
            </ListItem>
          </Link>

          <Link passHref href="/portals" aria-label="Portals page">
            <ListItem button component="a">
              portals
            </ListItem>
          </Link>

          <Link passHref href="/gold" aria-label="Gold page">
            <ListItem button component="a">
              gold
            </ListItem>
          </Link>

          <Link passHref href="/help" aria-label="Help page">
            <ListItem button component="a">
              help
            </ListItem>
          </Link>

          <Link passHref href="/about" aria-label="About page">
            <ListItem button component="a">
              about
            </ListItem>
          </Link>

          <Link passHref href="/terms" aria-label="Terms of Use page">
            <ListItem button component="a">
              terms of use
            </ListItem>
          </Link>

          <Link passHref href="/privacy" aria-label="Privacy Policy page">
            <ListItem button component="a">
              privacy policy
            </ListItem>
          </Link>

          <Link passHref href="/directory" aria-label="Directory page">
            <ListItem button component="a">
              directory
            </ListItem>
          </Link>
        </List>
      </Container>
    </>
  )
}
