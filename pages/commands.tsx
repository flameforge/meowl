/* eslint-disable react/no-unescaped-entities */

import { Avatar, Box, Button, Chip, Container, List, ListItem, ListItemAvatar, ListItemText, Typography, useMediaQuery, useTheme } from '@mui/material'
import Head from 'next/head'
import React from 'react'
import Meowing from '../assets/console/meowing'
import { convertPortalToMeow } from '../assets/console/thinking'
import miavatars from '../assets/utils/miavatars'
import { Context } from '../context'
import { meows, meows as originalMeows } from '../data/meows'
import { Portal, portals } from '../data/portals'

export async function getStaticProps() {
  const uniqueList = new Set()

  originalMeows.forEach((meow) => meow.phrases.forEach((phrase) => uniqueList.add(phrase)))
  portals.forEach((portal) => uniqueList.add(portal.name))

  const toSort = Array.from(uniqueList)
  const sorted = toSort.sort()

  return {
    props: {
      sorted,
      portals,
    },
  }
}

export default function Commands({ sorted, portals }): JSX.Element {
  const theme = useTheme()
  const biggerThanSm = useMediaQuery(theme.breakpoints.up('sm'))

  const { catsole } = React.useContext(Context)
  const [zen, setZen] = React.useState(false)

  React.useEffect(() => {
    setZen(catsole.length > 0)
  }, [catsole])

  return (
    <>
      <Head>
        <title>Meowl space </title>
        <meta name="description" content="Simple homepage to jump into the service that you want" />
      </Head>

      <Container component="header" maxWidth="md" sx={{ pt: 7 }}>
        <Typography variant="h1" align="center" gutterBottom>
          meows
        </Typography>

        <Typography variant="subtitle1" align="center" gutterBottom sx={{ opacity: zen ? 0 : 1, transition: 'opacity .2s ease' }}>
          Cats can be very vocal, especially when they're happy. ... A high-pitched meow is a content cat.
        </Typography>
      </Container>

      <Container maxWidth="md">
        <Meowing />
      </Container>

      <Container component="section" maxWidth="lg">
        <Box component="ul" sx={{ padding: 0, textAlign: 'center', lineHeight: 2.5, mb: 0 }}>
          {sorted.map((command, index) => (
            <Chip
              label={command}
              key={index}
              component="li"
              sx={{
                mr: 0.5,
                backgroundColor: 'rgba(0, 0, 0, 0.03)',
                transition: 'background-color 250ms, color 250ms',
                '&:hover': {
                  cursor: 'pointer',
                  color: theme.palette.primary.dark,
                },
              }}
            />
          ))}
        </Box>
      </Container>

      <Container component="main" maxWidth="md" sx={{ pt: 7 }}>
        <Typography variant="h2" align="center" gutterBottom>
          commands
        </Typography>

        <List sx={{ bgcolor: 'background.paper', padding: 0 }}>
          {meows.map(({ name, phrases, description, command }, index) => (
            <ListItem key={index} sx={{ maxWidth: '100%' }} divider dense={!biggerThanSm}>
              {biggerThanSm ? (
                <ListItemAvatar>
                  <Avatar alt={name} src={miavatars(index)} title={name} />
                </ListItemAvatar>
              ) : null}

              <ListItemText primary={name} secondary={description} />

              {biggerThanSm ? phrases.map((phrase, i) => <Chip label={phrase} key={i} sx={{ backgroundColor: 'rgba(0, 0, 0, 0.03)', mr: 0.5 }} />) : null}

              <Button size="small" variant="outlined" onClick={() => command()} sx={{ ml: 2 }}>
                Meow
              </Button>
            </ListItem>
          ))}
        </List>
      </Container>

      <Container component="section" maxWidth="md" sx={{ pt: 7, pb: 5 }}>
        <Typography variant="h2" align="center" gutterBottom>
          portals
        </Typography>

        <List sx={{ bgcolor: 'background.paper', padding: 0 }}>
          {portals
            .map((portal: Portal) => convertPortalToMeow(portal))
            .map(({ name, description, command, avatar }, index) => (
              <ListItem key={index} alignItems={!biggerThanSm ? 'flex-start' : 'center'} divider dense={!biggerThanSm}>
                <ListItemAvatar onClick={() => command()}>
                  <Avatar alt={name} src={avatar} />
                </ListItemAvatar>
                <ListItemText primary={name} secondary={description} />
                <>
                  {biggerThanSm ? (
                    <Box sx={{ height: '100%' }}>
                      <Button variant="outlined" onClick={() => command()} sx={{ ml: 2 }}>
                        Meow
                      </Button>
                    </Box>
                  ) : null}
                </>
              </ListItem>
            ))}
        </List>
      </Container>
    </>
  )
}
