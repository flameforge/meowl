import { Typography } from '@mui/material'
import Container from '@mui/material/Container'
import Head from 'next/head'
import React, { useEffect } from 'react'
import ReactMarkdown from 'react-markdown'
import rehypeSlug from 'rehype-slug'
import remarkGfm from 'remark-gfm'
import remarkParse from 'remark-parse'
import Meowing from '../assets/console/meowing'
import { Context } from '../context'
import terms from '../public/documents/terms.md'

export default function Terms(): JSX.Element {
  const { catsole } = React.useContext(Context)
  const [zen, setZen] = React.useState(false)

  useEffect(() => {
    setZen(catsole.length > 0)
  }, [catsole])

  return (
    <>
      <Head>
        <title>Meowl space</title>
        <meta name="description" content="Simple homepage to jump into the service that you want" />
      </Head>

      <Container component="header" maxWidth="md" sx={{ pt: 7 }}>
        <Typography component="div" variant="h1" align="center" gutterBottom>
          meow!
        </Typography>

        <Typography align="center" gutterBottom sx={{ opacity: zen ? 0 : 1, transition: 'opacity .2s ease' }}>
          A simple homepage for you to jump right into where you want.
        </Typography>
      </Container>

      <Container component="aside" maxWidth="md">
        <Meowing />
      </Container>

      <Container component="main" maxWidth="md" sx={{ pb: 3 }}>
        <ReactMarkdown remarkPlugins={[remarkParse, remarkGfm]} rehypePlugins={[rehypeSlug]}>
          {terms}
        </ReactMarkdown>
      </Container>
    </>
  )
}
