import { Box, Button, Card, CardActionArea, CardActions, CardContent, CardMedia, Grid, LinearProgress } from '@mui/material'
import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'
import { GetServerSideProps } from 'next'
import React, { Fragment, useEffect } from 'react'
import { useInView } from 'react-intersection-observer'
import { useInfiniteQuery } from 'react-query'
import { useUIDSeed } from 'react-uid'
import { youtubeImgApi } from '../assets/app/external'
import Meowing from '../assets/console/meowing'
import fetchData from '../assets/utils/fetchData'
import { Context } from '../context'
import { Nugget } from '../data/nuggets'

const fetchNuggets = async ({ pageParam = 1 }) => {
  const url = `/api/nuggets/?page=${pageParam}`
  const nuggets = await fetchData(url)
  return nuggets
}

export const getServerSideProps: GetServerSideProps = async () => {
  const data = await fetchNuggets({ pageParam: 1 })

  const nuggets = {
    pages: [{ data }],
    pageParams: [null],
  }

  return {
    props: {
      nuggets: nuggets,
    },
  }
}

function Gold(): JSX.Element {
  const { catsole } = React.useContext(Context)
  const [zen, setZen] = React.useState(false)

  useEffect(() => {
    setZen(catsole.length > 0)
  }, [catsole])

  const seed = useUIDSeed()
  const [ref, inView] = useInView({ threshold: 0 })

  const { data, isSuccess, fetchNextPage, hasNextPage, isLoading } = useInfiniteQuery('nuggets', fetchNuggets, {
    getNextPageParam: (stack) => (stack.page === stack.pages ? undefined : stack.page + 1),
    refetchOnWindowFocus: false,
  })

  useEffect(() => {
    if (hasNextPage) fetchNextPage()
  }, [fetchNextPage, hasNextPage, inView])

  return (
    <>
      <Container component="header" maxWidth="md" sx={{ pt: 7 }}>
        <Typography variant="h1" align="center" gutterBottom>
          cat gold
        </Typography>

        <Typography variant="subtitle1" align="center" gutterBottom sx={{ opacity: zen ? 0 : 1, transition: 'opacity .2s ease' }}>
          In fact, according to Bloomberg&quot;s The Magazine Trying to Bring the Web&quot;s Cat Obsession Offline, cats drive almost 15 percent of all Web
          traffic. There are also about 30 million Google searches per month for the search term &quot;cat&quot;.
        </Typography>
      </Container>
      <Container component="section" maxWidth="md">
        <Meowing />
      </Container>

      <Container maxWidth="lg" sx={{ mb: 3, mt: 4 }} component="main">
        <Grid container spacing={1}>
          {isSuccess &&
            data?.pages.map((page) => (
              <Fragment key={seed(page)}>
                {page.payload.map((item: Nugget, index) => (
                  <Grid item key={item.name} xs={6} sm={4} md={3}>
                    <Card key={index} sx={{ maxWidth: 345 }}>
                      <CardActionArea href={item.url} target="_blank" tabIndex={-1} rel="noreferrer">
                        <CardMedia
                          component="img"
                          alt={item.description}
                          image={item.type === 'video' ? youtubeImgApi + item.vid + '/0.jpg' : item.url}
                          height="140"
                          loading="lazy"
                        />
                        <CardContent>
                          <Typography gutterBottom variant="h5" component="div">
                            {item.name}
                          </Typography>
                          <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                            {item.author}
                          </Typography>
                          <Typography variant="body2" color="text.secondary">
                            {item.description}
                          </Typography>
                        </CardContent>
                        <CardActions>
                          <Button size="small">{item.button || 'Jump to'}</Button>
                        </CardActions>
                      </CardActionArea>
                    </Card>
                  </Grid>
                ))}
              </Fragment>
            ))}
        </Grid>

        <Box ref={ref} sx={{ mt: 3, mb: 3 }}>
          {isLoading ? <LinearProgress /> : ''}
        </Box>
      </Container>
    </>
  )
}

export default Gold
