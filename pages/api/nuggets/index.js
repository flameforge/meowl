import { nuggets } from '../../../data/nuggets'

export default async function handler({ method, query }, res) {
  const page = Number(query.page) || 1
  const limit = Number(query.limit) || 8
  const size = nuggets.length
  const pages = Math.ceil(size / limit)

  const start = page * limit - limit
  const end = start + limit
  const payload = [...nuggets].slice(start, end)

  const message = `Here, takes this ${payload.length} Nuggets`

  switch (method) {
    case 'GET':
      res.status(200).json({ message, payload, size, limit, page, pages })
      break
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end(`Method ${method} Not Allowed`)
  }
}
