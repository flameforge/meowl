import { nuggets } from '../../../data/nuggets'

export default async function handler(req, res) {
  const httpMethod = req.method
  const nuggetId = req.query.id
  const result = nuggets.filter((nugget) => nugget.id === nuggetId)

  const { id, type, title, description, url, author, vid, button } = req.body

  switch (httpMethod) {
    case 'GET':
      if (result.length > 0) {
        res.status(200).json(result[0])
      } else {
        res.status(404).json({ message: `Nugget with id ${nuggetId} Not Found` })
      }
      break
    case 'PUT':
      // Update the Nugget (TODO)
      res.status(200).json({
        id: id,
        type: type,
        title: title,
        description: description,
        url: url,
        author: author,
        vid: vid,
        button: button,
      })
      break
  }
}
