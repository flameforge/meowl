import { Container, Typography } from '@mui/material'
import Head from 'next/head'
import React, { useContext, useEffect, useState } from 'react'
import Meowing from '../assets/console/meowing'
import { Context } from '../context'

export default function Home(): JSX.Element {
  const { catsole } = useContext(Context)
  const [zen, setZen] = useState(false)

  useEffect(() => {
    setZen(catsole.length > 0)
  }, [catsole])

  return (
    <>
      <Head>
        <title>Meowl space</title>
        <meta name="description" content="Simple homepage to jump into the service that you want" />
      </Head>

      <Container component="header" maxWidth="md" sx={{ pt: 7 }}>
        <Typography variant="h1" align="center" gutterBottom>
          meow!
        </Typography>

        <Typography align="center" gutterBottom sx={{ opacity: zen ? 0 : 1, transition: 'opacity .2s ease' }}>
          A simple homepage for you to jump right into where you want.
        </Typography>
      </Container>

      <Container component="main" maxWidth="md">
        <Meowing />
      </Container>
    </>
  )
}
